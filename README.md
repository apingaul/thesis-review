# My thesis

The thesis is automatically rebuilt and uploaded on each commit:

- HTML version is available at <https://cern.ch/apingaul/thesis/>
- PDF version is available at <https://cern.ch/apingaul/thesis/thesis.pdf> (or click on the download button on the html page)

## Local build using the docker image

- Edit the env file `docker.env`, especially the path to book.
- Make a symlink to `.env`: `ln -s docker.env .env`
- Run the container:  
    ```bash
    docker-compose up -d thesisdown
    ```
- Render the document:
  - From rstudio (availiable at http://127.0.0.1:8787 once the container is running), by clicking on knit.
  - From the command line: `make docker-pdf`
