RCMD = Rscript -e
USER = rstudio
CONTAINER = thesisdown

pdf:
	$(RCMD) 'bookdown::render_book("index.Rmd", output_format = "thesisdown.ugent::thesis_pdf")'
	rm -f *.mtc* *.maf *.aux *.bcf *.lof *.lot *.out *.toc *.tcc* *.wrt

gitbook:
	$(RCMD) 'bookdown::render_book("index.Rmd", output_format = "thesisdown.ugent::thesis_gitbook")'
	# $(RCMD) 'bookdown::render_book("index.Rmd", output_format = "bookdown::gitbook")'

preview:
	$(RCMD) 'bookdown::preview_chapter("0$(CHAPTER)-chap$(CHAPTER).Rmd", "thesisdown.ugent::thesis_pdf")'

serve:
	$(RCMD) 'bookdown::serve_book("_book")'
	# $(RCMD) 'bookdown::serve_book(".", daemon=TRUE)'
	# rm -f *.log *.mtc* *.maf *.aux *.bcf *.lof *.lot *.out *.toc
	# Rscript -e 'browseURL("_book/thesis.pdf")'

clean:
	$(RCMD) "bookdown::clean_book(TRUE)"
	rm -f *.mtc* *.maf *.aux *.bcf *.lof *.lot *.out *.toc *.tcc* *.wrt
	rm -f thesis.Rmd *.log

clean-knits:
	make clean
	rm -f *.docx *.html *.pdf *.log *.maf *.mtc*
	rm -R *_files
	rm -R *_cache

docker:
	docker exec -u $(USER) $(CONTAINER) $(CMD)

docker-pdf:
	make docker CMD="make pdf"

docker-gitbook:
	make docker CMD="make gitbook"

docker-serve:
	make docker CMD="make serve"
