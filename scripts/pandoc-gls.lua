-- Original from https://github.com/tomncooper/pandoc-gls
function replace(front, ac, back, command)
  if FORMAT:match 'latex' then
    -- replace markup with proper glossaries command
    new_str = string.format("%s\\%s{%s}%s", front, command, ac, back)
    return pandoc.RawInline("tex", new_str)
  else
    -- just remove the markup
    span_text = string.format("%s%s%s", front, ac, back)
    span_attr = pandoc.Attr("", {}, {{"style", "color: red;"}})
  return pandoc.Span(span_text, span_attr)
  --   span = pandoc.Span(span_text, span_attr)
  --   foot_note = pandoc.Note(pandoc.Para('glossary items are not defined in html output, see glossary in pdf for more info'))
  -- return {span, foot_note}
  end
end

function Str(el)

    front, capital, plural, ac, apostrophe, back = el.text:match(
        "(%g*)%(([%+%-]+)(%^-)(%w+[_%-]*%w*)%)([%\u{0027}%\u{2019}]*)(%g*)"
    )

    if ac ~= "" then
        if plural == "^" then
            if capital == "++" then
                command = "Glspl"
            elseif capital == "+" then
                command = "glspl"
            elseif capital == "-+" then
                command = "Glsentryplural"
            elseif capital == "-" then
                command = "glsentryplural"
            else
                -- Unknown command string so just return the element unchanged
                return el
            end
        else
            if capital == "++" then
                command = "Gls"
            elseif capital == "+" then
                command = "gls"
            elseif capital == "-+" then
                command = "Glsentryname"
            elseif capital == "-" then
                command = "glsentryname"
            else
                -- Unknown command string so just return the element unchanged
                return el
            end
        end

        if apostrophe == "\u{0027}" or apostrophe == "\u{2019}" then
            if back ~= "" then
                back = string.format("'%s", back)
            else
                back = "\\textquotesingle\\space"
            end
        end

        return replace(front, ac, back, command)
    end
end
